﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;


public class Bullet2Script : MonoBehaviour {
    private AudioSource sound;
    int i;
	// Use this for initialization
	void Start () {
        sound = gameObject.GetComponent<AudioSource>();
        i = 0;
	}
	
	// Update is called once per frame
	void Update () {
        i++;
        if (i == 100)
        {
            Destroy(this);
        }
	}
private void OnCollisionEnter(Collision col)
    {
        if (col.gameObject)
        {
            Destroy(gameObject);
        }
        if (col.gameObject.CompareTag("enemy"))
        {
            Destroy(gameObject);
            Destroy(col.gameObject);
            sound.Play();
        }
    }
}
