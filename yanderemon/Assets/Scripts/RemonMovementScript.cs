﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RemonMovementScript : MonoBehaviour {
    List<Vector3> pos;
	// Use this for initialization
	void Start () {
        pos = new List<Vector3>(4);
        pos.Add(new Vector3(100, 0, 0));
        pos.Add(new Vector3(-100, 0, 0));
        pos.Add(new Vector3(0, 0, 100));
        pos.Add(new Vector3(0, 0, -100));
        transform.GetComponent<Rigidbody>().AddForce(pos[Random.Range(0,pos.Count)],ForceMode.Impulse);
	}
	
	// Update is called once per frame
	void Update () {
		
	}
}
