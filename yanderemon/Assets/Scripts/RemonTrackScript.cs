﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RemonTrackScript : MonoBehaviour {
    GameObject player;
    Vector3 pos, pos1, pos2;
    int i;
	// Use this for initialization
	void Start () {
        player = GameObject.FindWithTag("Player");
        i = 0;
;	}
	
	// Update is called once per frame
	void Update () {
        pos = player.transform.position;
        pos1 = transform.position;
        pos2 = (pos - pos1)/10;
        transform.GetComponent<Rigidbody>().AddForce(pos2,ForceMode.VelocityChange);
        i++;
        if (i > 400)
        {
            Destroy(gameObject);
        }
	}
}
