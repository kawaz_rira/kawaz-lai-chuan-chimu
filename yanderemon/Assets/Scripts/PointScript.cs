﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class PointScript : MonoBehaviour {
    Slider slider;
    GameObject ver,player;
    public GameObject clear;
        // Use this for initialization
	void Start () {
        ver = GameObject.Find("Point");
        slider = ver.GetComponent<Slider>();
        player = GameObject.Find("Player");
	}
	
	// Update is called once per frame
	void Update () {
        if (slider.value == 1)
        {
            clear.SendMessage("Clear");
            player.SendMessage("False");
        }
	}
    void get()
    {
        slider.value += 0.05f;
    }
}
