﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Audio2Script : MonoBehaviour {
    private AudioSource audio;
    public GameObject player,insta;
    
    int n;
	// Use this for initialization
	void Start () {
        audio = gameObject.GetComponent<AudioSource>();
        audio.Play();
        
        n = 0;
	}
	
	// Update is called once per frame
	void Update () {
        n++;
        if (n > 100)
        {
            player.SendMessage("awake");
            insta.SendMessage("awake");
        }
    }
}
