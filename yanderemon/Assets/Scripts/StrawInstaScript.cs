﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class StrawInstaScript : MonoBehaviour {
    [SerializeField]
    public GameObject straw;
    List<Vector3> pos_col;
    public int n,l;
      bool  i;
	// Use this for initialization
	void Start () {
        n = 0;
        l = 0;
        i = false;
	}
	
	// Update is called once per frame
	void Update () {
        l++; ;
        if (l > 100)
        {
            i = true;
        }

        if (i)

        {
            n++;
        }
        switch (n)
        {
            case 10:
                int index = Random.Range(0, pos_col.Count);
                GameObject remonTemp = Instantiate(straw);
                remonTemp.transform.position = pos_col[index];
                pos_col.RemoveAt(index);
                break;
            case 11:
                int index1 = Random.Range(0, pos_col.Count);
                GameObject remonTemp1 = Instantiate(straw);
                remonTemp1.transform.position = pos_col[index1];
                pos_col.RemoveAt(index1);
                break;
            case 12:
                int index2 = Random.Range(0, pos_col.Count);
                GameObject remonTemp2 = Instantiate(straw);
                remonTemp2.transform.position = pos_col[index2];
                pos_col.RemoveAt(index2);
                break;
            case 13:
                int index3 = Random.Range(0, pos_col.Count);
                GameObject remonTemp3 = Instantiate(straw);
                remonTemp3.transform.position = pos_col[index3];
                pos_col.RemoveAt(index3);
                break;
        }
        if (n > 500)
        {
            n = 0;
            InitializeRemonPos();
        }
    }
    void InitializeRemonPos()
    {
        pos_col = new List<Vector3>();
        pos_col.Add(new Vector3(60, 5, 50));
        pos_col.Add(new Vector3(60, 5, 0));
        pos_col.Add(new Vector3(60, 5, -50));
        pos_col.Add(new Vector3(-60, 5, 50));
        pos_col.Add(new Vector3(-60, 5, 0));
        pos_col.Add(new Vector3(-60, 5, -50));
        pos_col.Add(new Vector3(0, 5, 50));
        pos_col.Add(new Vector3(0, 5, 0));
        pos_col.Add(new Vector3(0, 5, -50));
    }
    void awake()
    {
        i = true;
    }
    void False()
    {
        i = false;
    }
}
