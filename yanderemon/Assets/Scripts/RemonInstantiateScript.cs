﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RemonInstantiateScript : MonoBehaviour
{
    [SerializeField]
    public GameObject remon;
    [SerializeField]
    public GameObject remon1;
    [SerializeField]
    public GameObject remon2;
    List<Vector3> pos_col;
    List<GameObject> remon_col;
    public int n;
    bool i;
    // Use this for initialization
    void Start()
    {
        InitializeRemonPos();
        remon_col = new List<GameObject>(3);
        remon_col.Add(remon);
        remon_col.Add(remon1);
        remon_col.Add(remon2);
        n = 0;
        i = false;
    }

    // Update is called once per frame
    void Update()
    {
        if (i)
        {
            n++;
        }
        switch(n)
        {
            case 10:
                int index = Random.Range(0, pos_col.Count);
                GameObject remonTemp = Instantiate(remon_col[Random.Range(0,remon_col.Count)]);
                remonTemp.transform.position = pos_col[index];
                pos_col.RemoveAt(index);
                break;
            case 11:
                int index1 = Random.Range(0, pos_col.Count);
                GameObject remonTemp1 = Instantiate(remon_col[Random.Range(0, remon_col.Count)]);
                remonTemp1.transform.position = pos_col[index1];
                pos_col.RemoveAt(index1);
                break;
            case 12:
                int index2 = Random.Range(0, pos_col.Count);
                GameObject remonTemp2 = Instantiate(remon_col[Random.Range(0, remon_col.Count)]);
                remonTemp2.transform.position = pos_col[index2];
                pos_col.RemoveAt(index2);
                break;
            case 13:
                int index3 = Random.Range(0, pos_col.Count);
                GameObject remonTemp3 = Instantiate(remon_col[Random.Range(0, remon_col.Count)]);
                remonTemp3.transform.position = pos_col[index3];
                pos_col.RemoveAt(index3);
                break;
        }
        if (n > 500)
        {
            n = 0;
            InitializeRemonPos();
        }
    }

    void InitializeRemonPos()
    {
        pos_col = new List<Vector3>();
        pos_col.Add(new Vector3(60, 5, 50));
        pos_col.Add(new Vector3(60, 5, 0));
        pos_col.Add(new Vector3(60, 5, -50));
        pos_col.Add(new Vector3(-60, 5, 50));
        pos_col.Add(new Vector3(-60, 5, 0));
        pos_col.Add(new Vector3(-60, 5, -50));
        pos_col.Add(new Vector3(0, 5, 50));
        pos_col.Add(new Vector3(0, 5, 0));
        pos_col.Add(new Vector3(0, 5, -50));
    }
    void awake()
    {
        i = true;
    }
void False()
    {
        i = false;
    }
}


