﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class TimeScript : MonoBehaviour {
    int n,l,t;
    float m;
    Text text;
    string s,a;
	// Use this for initialization
	void Start () {
        n = 0;
        m = 0;
        l = 00;
        t = 2;
	}
	
	// Update is called once per frame
	void Update () {
        m += Time.deltaTime;
        if (m > 1)
        {
            l -= 1;
            m = 0;
        }
        if (l == 00)
        {
            t -= 1;
            l = 59;
        }
        
        s = l.ToString();
        a = t.ToString();
        GetComponent<Text>().text = a+":"+s;
	}
}
