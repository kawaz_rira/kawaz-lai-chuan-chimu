﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class StrawberryScript : MonoBehaviour {
    GameObject point;
    AudioSource audio;
	// Use this for initialization
	void Start () {
        point = GameObject.Find("Point");
        
	}
	
	// Update is called once per frame
	void Update () {
		
	}
    private void OnCollisionEnter(Collision collision)
    {
        if (collision.gameObject.CompareTag("Player"))
        {
            point.SendMessage("get");
            Destroy(gameObject);
            audio.Play();
        }
    }
}
