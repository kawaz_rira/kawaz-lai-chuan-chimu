﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BulletScript : MonoBehaviour {
    [SerializeField]
    public GameObject bullet;
    Vector3 pos,pos1;
    AudioSource audio;
	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
        if (Input.GetKeyDown(KeyCode.UpArrow))
        {
            pos = new Vector3(0, 0, 100);
            pos1 = new Vector3(0, 0, 10);
            
                GameObject bul = Instantiate(bullet);
                bul.transform.position = pos1+transform.position;
                bul.transform.GetComponent<Rigidbody>().AddForce(pos, ForceMode.Impulse);
            audio.Play();
            
        }
        if (Input.GetKeyDown(KeyCode.DownArrow))
        {
            pos = new Vector3(0, 0, -100);
            pos1 = new Vector3(0, 0, -10);
            
                GameObject bul = Instantiate(bullet);
                bul.transform.position = pos1+transform.position;
                bul.transform.GetComponent<Rigidbody>().AddForce(pos, ForceMode.Impulse);
            audio.Play();
            
        }
        if (Input.GetKeyDown(KeyCode.LeftArrow))
        {
            pos = new Vector3(-100, 0, 0);
            pos1 = new Vector3(-10, 0, 0);
            
                GameObject bul = Instantiate(bullet);
                bul.transform.position = transform.position+pos1;
                bul.transform.GetComponent<Rigidbody>().AddForce(pos, ForceMode.Impulse);
            audio.Play();
        }
        if (Input.GetKeyDown(KeyCode.RightArrow))
        {
            pos = new Vector3(100, 0, 0);
            pos1 = new Vector3(10, 0, 0);
            
                GameObject bul = Instantiate(bullet);
                bul.transform.position = transform.position+pos1;
                bul.transform.GetComponent<Rigidbody>().AddForce(pos, ForceMode.Impulse);
            audio.Play();
        }
        
	}
}
