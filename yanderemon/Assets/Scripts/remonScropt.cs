﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class remonScropt : MonoBehaviour {
    GameObject player;
    GameObject ver;
    Vector3 pos1;
    AudioSource audio;
	// Use this for initialization
	void Start () {
        player = GameObject.FindWithTag("Player");
        ver = GameObject.Find("HP");
        

    }
	
	// Update is called once per frame
	void Update () {
        
    }
    private void OnCollisionEnter(Collision collision)
    {
        if (collision.gameObject.CompareTag("Player"))
        {
            player.SendMessage("decrease");
            ver.SendMessage("damage");
            Destroy(gameObject);
            audio.Play();
        }
    }
}
