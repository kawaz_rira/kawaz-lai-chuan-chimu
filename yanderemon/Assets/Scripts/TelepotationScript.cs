﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TelepotationScript : MonoBehaviour {
    GameObject player;
	// Use this for initialization
	void Start () {
        player = GameObject.FindWithTag("Player");
	}
	
	// Update is called once per frame
	void Update () {
		
	}
    private void OnTriggerEnter(Collider other)
    {
        if (other.gameObject.CompareTag("Player"))
        {
            player.SendMessage("Teles");
        }
    }
}
