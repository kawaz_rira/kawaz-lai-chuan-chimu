﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ClearScript : MonoBehaviour {
    
	// Use this for initialization
	void Start () {
        transform.GetComponent<Text>().enabled = false;
	}
	
	// Update is called once per frame
	void Update () {
		
	}
void Clear()
    {
        transform.GetComponent<Text>().enabled = true;
    }
}
