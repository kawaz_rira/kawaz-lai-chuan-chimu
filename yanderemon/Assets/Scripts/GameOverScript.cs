﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class GameOverScript : MonoBehaviour {
    
	// Use this for initialization
	void Start () {
        transform.GetComponent<Text>().enabled = false;
	}
	
	// Update is called once per frame
	void fal () {
        transform.GetComponent<Text>().enabled = true;
	}
}
